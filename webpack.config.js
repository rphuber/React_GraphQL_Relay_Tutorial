import webpack from 'webpack';
import path from 'path';

export default {
	context: path.join(__dirname, './client/source'),
	entry: [
		'webpack-hot-middleware/client?reload=true',
		'./js/index.js'
	],
	output: {
		path: __dirname + '/build/js',
		filename: 'bundle.js',
		publicPath: '/js'
	},
	devServer: {
		contentBase: './build/js'
	},
	plugins: [
		new webpack.HotModuleReplacementPlugin(),
		new webpack.NoErrorsPlugin()
	],
	module: {
		loaders: [
			{ 
				test: /\.js$/, 
				include: [
					path.join(__dirname, './client'), 
					path.join(__dirname, './node_modules/@bfui/boomerang-ui-components/')
				], 
				loader: 'babel' },
			{ 
				test: /\.css$/, 
				include: [
					path.join(__dirname, './client'),
					path.join(__dirname, './node_modules/@bfui/boomerang-ui-components')
				],
				loaders: [
					'style-loader',
					'css-loader?sourceMap&modules&importLoaders=1&localIdentName=[name]__[local]___[hash:base64:5]!postcss-loader?sourceMap'
				]
			}
		]
	},
	resolve: {
		fallback: path.join(__dirname, 'node_modules')
	},
	resolveLoader: {
		fallback: path.join(__dirname, 'node_modules')
	}
}
